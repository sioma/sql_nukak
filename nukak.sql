create schema public;

create table backups
(
    backup_id serial not null
        constraint backups_pkey
            primary key,
    fecha     date,
    tipo      text
);

alter table backups
    owner to postgres;

-- auto-generated definition
create table validacions
(
    validacion_id  serial not null
        constraint validacions_pk
            primary key,
    fecha          timestamp,
    calibracion_on varchar,
    error          double precision,
    correcto       integer,
    ratio          double precision,
    peso           double precision,
    estado         integer,
    cero           double precision,
    estoma_id      varchar,
    persona_id     integer,
    patron double precision
);

alter table validacions
    owner to postgres;

create table estomas (
    estoma_id integer not null constraint estomas_pk primary key,
    nombre varchar,
    tolerancia_validacion integer,
    password_calibracion varchar,
    patron double precision
);
alter table estomas owner to postgres;

create table parametros
(
    parametro_id serial not null
        constraint parametros_pkey
            primary key,
    parametro    text
        constraint parametros_parametro_key
            unique,
    fecha        text,
    valor        text
);

alter table parametros
    owner to postgres;

create table bandejas
(
    bandeja_id    serial not null
        constraint bandejas_pkey
            primary key,
    fecha         timestamp,
    peso          text,
    sobrepeso     real,
    embarque_id   text,
    tara          text,
    cero          text,
    estoma_id     integer,
    persona_id    text,
    subido        integer
);

alter table bandejas
    owner to postgres;

create table embarques
(
    id          serial not null
        constraint embarques_pkey
            primary key,
    embarque_id text,
    codigo      text,
    nombre      text,
    peso_kg     real,
    peso_lb     real,
    activo      integer default 0,
    sobrepeso_tolerable double precision ,
    tara        decimal(6,3) default 0,
    updated_at  timestamp,
    created_at  timestamp
);

alter table embarques
    owner to postgres;

create table personas
(
  persona_id integer not null
    constraint personas_pk
    primary key,
  nombre     varchar not null,
  apellidos  varchar not null,
  codigo     varchar not null
);

alter table personas
  owner to postgres;


create table wifis
(
    wifi_id serial not null
        constraint wifis_pk
            primary key,
    ssid     varchar,
    psw     varchar
);

alter table wifis
    owner to postgres;
